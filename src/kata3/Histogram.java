package kata3;

/**
 * Created by Spiess-Knafl on 10/13/14.
 */
import java.util.HashMap;

public class Histogram<Type> extends HashMap<Type,Integer> {

    @Override
    public Integer get(Object key) {
        if (this.containsKey(key))
            return super.get(key);
        return 0;
    }
}
