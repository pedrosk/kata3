package kata3;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by Spiess-Knafl on 10/13/14.
 */
public class MailReader {

    private final String filePath;

    public MailReader(String filePath) {
        this.filePath = filePath;
    }

    public String[] readDomains() {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(filePath));
            ArrayList<String> domainList = new ArrayList<>();

            while(true) {
                String line = reader.readLine();
                if (line == null) break;
                if(line.contains("@"))
                    domainList.add(line.split("@")[1]);
            }

            return domainList.toArray(new String[domainList.size()]);

        } catch (IOException e) {
            return new String[0];
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                Logger.getLogger(MailReader.class.getName(), e.getMessage());

            }

        }
    }
}
